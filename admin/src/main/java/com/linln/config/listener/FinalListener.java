package com.linln.config.listener;

import com.linln.common.constant.ParamConst;
import com.linln.modules.system.domain.Param;
import com.linln.modules.system.repository.ParamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;



@Slf4j
@Component
@Order
public class FinalListener implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private ParamRepository paramRepository;
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        Param nbParam = paramRepository.findFirstByName(ParamConst.INIT_STATUS);
        if (nbParam != null) {
          //  nbParam.setValue(ParamConst.INIT_SURE);
            paramRepository.updateValue(ParamConst.INIT_SURE,nbParam.getId());
        }


        log.info("「小懒虫」App 启动完毕。");
    }
}
