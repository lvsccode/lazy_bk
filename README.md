<<<<<<< HEAD




#### 项目介绍

小海豚博客基于小懒虫后台管理系统，用到技术 SpringBoot2.0 + Spring Data Jpa + Thymeleaf + Shiro 开发的后台管理系统，采用分模块的方式便于开发和维护，目前支持的功能有：权限管理、部门管理、字典管理、日志记录、文件上传、代码生成
，博客模块，系统参数模块等！

#### QQ群  刚建： 321225959
#### 技术选型


- 后端技术：SpringBoot + Spring Data Jpa + Thymeleaf + Shiro + Jwt + EhCache

- 前端技术：前端技术：Layui + Font-awesome + nkeditor

#### 博客预览
[前端预览](https://www.nonelonely.com)
[后台预览](https://www.nonelonely.com/login)  （账号/密码  test/test）
#### 后台图片预览（地址预览在上面，目前只有开放查看权限）
![首页](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/主页.png)
![博文管理](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/博文管理.png)
![写博文](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/写博文.png)
![用户管理](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/用户管理.png)
![菜单管理](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/菜单管理.png)
![角色管理](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/角色管理.png)
![访客分布](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/访客分布.png)
![访客记录](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/访客记录.png)
#### 博客官网

[小海豚博客](https://www.nonelonely.com/frontend/dolphi)

#### 全新的项目结构

![项目结构图](https://images.gitee.com/uploads/images/2020/0225/132322_7c0689b9_1165306.png)

#### 功能列表

- 用户管理：用于管理后台系统的用户，可进行增删改查等操作。
- 角色管理：分配权限的最小单元，通过角色给用户分配权限。
- 菜单管理：用于配置系统菜单，同时也作为权限资源。
- 部门管理：通过不同的部门来管理和区分用户。
- 字典管理：对一些需要转换的数据进行统一管理，如：男、女等。
- 行为日志：用于记录用户对系统的操作，同时监视系统运行时发生的错误。
- 文件上传：内置了文件上传接口，方便开发者使用文件上传功能。
- 代码生成：可以帮助开发者快速开发项目，减少不必要的重复操作，花更多精力注重业务实现。
- 表单构建：通过拖拽的方式快速构建一个表单模块。
- 系统参数管理：设置网站一些值，如网站名称等等
- 博客系统管理：包括博文，笔记，标签，评论，类别管理等等
- 前端博客模块 ：是一个响应式博客前端，美化，简约的风格
- 定时任务调度模块：更容易开发的定时任务，可以随时管理定时任务的状态，如时间，启动，关闭，是否开机就启动等。
- 系统环境监测：使用实时查看系统的cpu,内存状态，可以获取系统的各种配置信息包括网卡，MAC等
- 后台系统查看实时日志：后台用webSocket实现增加实时查看日志功能，再也不用登录linux系统获取日志文件了
- 邮箱系统：（已完成收件功能）
- 插件化管理：待开发

#### 安装教程

- ##### 环境及插件要求

   - Jdk8+
   - Mysql5.5+
   - Maven
   - Lombok<font color="red">（重要）</font>

- ##### 导入项目

   - IntelliJ IDEA：Import Project -> Import Project from external model -> Maven
   - Eclipse：Import -> Exising Mavne Project
  

- ##### 运行项目
  - 创建一个数据库test就行，表不用创建
  - 数据库配置：数据库名称test   用户xxxxx    密码xxxxx
   - 通过Java应用方式运行admin模块下的com.linln.BootApplication.java文件
   - 先运行程序，生成表,再导入数据库文件 data.sql  在工程目录下查找
  - 访问地址：http://localhost:8080/
  - 后台默认帐号密码：admin/123456



#### 更新记录
- 2020/4/14
1.优化系统参数管理，增加分类，更容易管理
![系统参数](https://gitee.com/linping0124/lazy_bk/raw/master/sdoc/images/系统参数.png)
- 2020/3/7 
1.修复后台上传头像时，错误的问题
- 2020/2/29
1.增加小海豚官网<br>
2.增加初始化数据库时导入初始数据。真正实现只要创建一个库就可执行。<br>
3.增加邮箱验证邮箱号，设置邮箱的用户可以自动收到被回复的信息
- 2020-2-24更新  
   1.增加博客前端模块  预览地址：[小海豚博客](https://www.nonelonely.com)<br>
   2.小懒虫后台管理系统更名为小海豚博客系统
- 2019-1-6更新  
  1.增加SSH配置，http会自动转成https<br>
  2.配置信息：<br>
  
     > server:
        port: 443  #监听到http的端口号后转向到的https的端口号，默认443  若没有ssl配置，更改端口即更改此属性即可<br>
        ssl:<br>
          enabled: true #网站是否启用ssl，默认false（不启用）<br>
          key-store: classpath:aliyu.pfx   #启用了ssl之后需要配置证书，以下的配置为证书配置<br>
          key-store-password: 123456    #证书密码 用自己的<br>
          key-store-type: PKCS12   #证书类型，默认jks，如需要ssl配置，放开此配置项属性即可 阿里云的是PKCS12<br>
        http:<br>
          port: 8080  #项目监听的http的端口号，默认80  若没有ssl配置，则这个参数无效<br>
          
  3.阿里云SSH免费证书申请教程： [https://nonelonely.com/article/1556780360345](https://nonelonely.com/article/1556780360345)
- 2019-1-5更新  
1.增加Url层面的权限系统<br>
2.增加注解，由于注解你要控制的url<br>
 3.增加系统启动，自动初始化参数，如角色，管理员账号，系统参数等一些必要信息。<br>
 4.增加了QQ登录模块<br>
 5.剔除zTree代码<br>
 6.优化代码生成模块，适应URl权限的扫描<br>
 7.优化验证码验证，可用后台系统参数captcha_open控制
- 2019-1-2更新  
1.加入博客系统<br>
2.包括：评论，笔记，博文，类别，标签等功能<br>
 3.修改后台系统名称为小懒虫
- 2019-12-31更新 
正式发布v1.0系统<br>
1.基于TIMO后台管理系统<br>
2.增加系统参数功能，前台可以用[[${#params.value(参数名称)}}]]使用<br>
3.去除ztree插件，使用layui的tree.<br>
4.增加图标的点击选择

