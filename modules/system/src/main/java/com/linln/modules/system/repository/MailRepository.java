package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Mail;
import com.linln.modules.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author 小懒虫
 * @date 2020/04/19
 */
public interface MailRepository extends BaseRepository<Mail, Long> {

    Mail findByMessageNumber(int ms);

    @Query(nativeQuery = true,value = "select ifnull(MAX(message_number),0) from or_mail ")
    int findMessageNumber();

    int countByIsSeen(boolean isSeen);
}