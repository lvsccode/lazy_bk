/*
Navicat MySQL Data Transfer

Source Server         : 119.23.241.23
Source Server Version : 50724
Source Host           : 119.23.241.23:3306
Source Database       : test2

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-04-23 21:29:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for or_param
-- ----------------------------
DROP TABLE IF EXISTS `or_param`;
CREATE TABLE `or_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `value` longtext,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK56ar3u2j131f46ntlyo0bqunb` (`create_by`),
  KEY `FK4x1vfkrolx98y2pia26c38ykw` (`update_by`),
  CONSTRAINT `FK4x1vfkrolx98y2pia26c38ykw` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK56ar3u2j131f46ntlyo0bqunb` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of or_param
-- ----------------------------
INSERT INTO `or_param` VALUES ('1', '2020-01-09 12:16:03', 'init_status', '标记用户是否在「小懒虫」App 的初始化设置页面设置过', '1', '2020-04-13 23:57:28', '1', '1', '1', null, '40', null);
INSERT INTO `or_param` VALUES ('2', '2020-01-09 12:16:05', 'website_title', '网站标题的文字', '1', '2020-04-14 21:26:20', '小海豚个人博客', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('3', '2020-01-09 12:16:05', 'footer_words', '页脚的文字', '1', '2020-04-14 21:26:33', '<p> <strong>2018-2020 nonelonely.com | 网站备案号：<a href=\"http://www.beian.miit.gov.cn\">闽ICP备18026034号-1</a></strong> </p>', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('4', '2020-01-09 12:16:05', 'index_top_words', '首页置顶文字', '3', '2020-01-09 12:16:05', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('5', '2020-01-09 12:16:06', 'wechat_pay', '微信付款码', '3', '2020-01-09 12:16:06', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('6', '2020-01-09 12:16:06', 'alipay', '支付宝付款码', '3', '2020-01-09 12:16:06', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('7', '2020-01-09 12:16:06', 'info_label', '信息板内容', '3', '2020-01-09 12:16:06', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('8', '2020-01-09 12:16:07', 'website_logo_words', '网站logo的文字', '1', '2020-04-14 23:31:00', '小海豚个人博客', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('9', '2020-01-09 12:16:07', 'website_logo_small_words', '网站logo的文字旁的小字', '1', '2020-04-14 23:30:30', '小海豚上线了啊', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('10', '2020-01-09 12:16:07', 'comment_notice', '评论置顶公告', '3', '2020-01-09 12:16:07', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('11', '2020-01-09 12:16:08', 'project_top_notice', '项目置顶公告', '3', '2020-01-09 12:16:08', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('12', '2020-01-09 12:16:08', 'message_panel_words', '留言板的提示信息文字', '1', '2020-04-14 23:36:54', '<span>本页留言暂时关闭，特此通知！</span>', '1', '1', null, '45', null);
INSERT INTO `or_param` VALUES ('13', '2020-01-09 12:16:08', 'mail_smpt_server_addr', 'SMTP服务器', '1', '2020-04-14 21:29:34', 'smtp.nonelonely.com', '1', '1', null, '42', null);
INSERT INTO `or_param` VALUES ('14', '2020-01-09 12:16:08', 'mail_smpt_server_port', 'SMTP端口号', '1', '2020-04-14 21:29:48', '465', '1', '1', null, '42', null);
INSERT INTO `or_param` VALUES ('15', '2020-01-09 12:16:09', 'mail_server_account', '发件人邮箱', '1', '2020-04-14 21:30:00', 'admin@nonelonely.com', '1', '1', null, '42', null);
INSERT INTO `or_param` VALUES ('16', '2020-01-09 12:16:09', 'mail_sender_name', '发件人邮箱帐号（一般为@前面部分）', '1', '2020-04-14 21:30:20', 'admin@nonelonely.com', '1', '1', null, '42', null);
INSERT INTO `or_param` VALUES ('17', '2020-01-09 12:16:09', 'mail_server_password', '邮箱登入密码', '1', '2020-04-14 21:30:09', '', '1', '1', null, '42', null);
INSERT INTO `or_param` VALUES ('18', '2020-01-09 12:16:10', 'app_id', 'qq登录API的app_id', '1', '2020-04-14 21:34:56', '', '1', '1', null, '43', null);
INSERT INTO `or_param` VALUES ('19', '2020-01-09 12:16:10', 'app_key', 'qq登录API的app_key', '1', '2020-04-14 21:52:45', '', '1', '1', null, '43', null);
INSERT INTO `or_param` VALUES ('20', '2020-01-09 12:16:10', 'system_name', '后台系统名称', '1', '2020-04-14 23:31:26', '小海豚', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('21', '2020-01-09 12:16:10', 'captcha_open', '验证码是否开启', '1', '2020-04-14 21:55:59', 'true', '1', '1', null, '43', null);
INSERT INTO `or_param` VALUES ('22', '2020-02-12 15:22:50', '', '', '3', '2020-02-12 15:22:50', '11', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('23', '2020-02-12 16:05:39', 'about_us_title', '关于我的标题', '1', '2020-04-14 23:29:11', '<span>当你觉得生活没意思，做什么都提不起兴趣时，说明你已经许久没有做出过改变了。长时间待在舒适区，人难免会慢慢丧失斗志，动起来，做出改变，出去看看世界或者学一项新技能，让自己始终在进步。你要相信，在这个世界上，一定有着另外一个你自己，在做着你不敢做的事，过着你想过的生活。</span>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('24', '2020-02-12 16:14:14', 'lot_title', '首页热门网站标题', '1', '2020-04-14 23:31:44', '每个人都有一个习惯，我的习惯是在这里等你来临。\r\n<br>', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('25', '2020-02-12 19:34:17', 'detail_notice', '博文页面的提示信息', '1', '2020-04-14 23:37:07', '温馨提示：本站所有文章，若非特别声明，均为原创，转载请注明作者及原文链接。', '1', '1', null, '45', null);
INSERT INTO `or_param` VALUES ('26', '2020-02-13 12:03:36', 'all_comment_open', '是否全局开放评论', '1', '2020-04-14 23:37:15', '1', '1', '1', null, '45', null);
INSERT INTO `or_param` VALUES ('27', '2020-02-13 12:04:38', 'comment_keyword', '评论关键字过滤', '1', '2020-04-14 23:37:26', '尼玛|我擦', '1', '1', null, '45', null);
INSERT INTO `or_param` VALUES ('28', '2020-02-13 20:10:32', 'website_info', '网站简介', '1', '2020-04-19 22:59:59', '<p>\r\n	<span>由于一开始之前的博客被人攻击，导致了好多数据没了，后来虽然找回了一些数据，但是回顾一下之前的博客系统很不满意，代码风格很冷，所以后面直接开始重构代码，其实就是重新开发，主要用了</span><span>&nbsp;SpringBoot2.0 + Spring Data Jpa + Thymeleaf + Shiro 开发的后台管理系统，采用分模块的方式便于开发和维护，目前支持的功能有：权限管理、部门管理、字典管理、日志记录、文件上传、代码生成 ，博客模块，系统参数模块等！有了代码生成对于后面开发前端数据很方便，所以我前端模板也重构了</span> \r\n</p>\r\n<h1>\r\n	技术选型\r\n</h1>\r\n<p>\r\n	<br />\r\n</p>\r\n<ol>\r\n	<li>\r\n		后端技术：SpringBoot + Spring Data Jpa + Thymeleaf + Shiro + Jwt + EhCache\r\n	</li>\r\n	<li>\r\n		前端技术：Layui + Font-awesome + nkeditor\r\n	</li>\r\n</ol>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	功能列表\r\n</h1>\r\n<p>\r\n	<br />\r\n</p>\r\n<ul>\r\n	<li>\r\n		用户管理：用于管理后台系统的用户，可进行增删改查等操作。\r\n	</li>\r\n	<li>\r\n		角色管理：分配权限的最小单元，通过角色给用户分配权限。\r\n	</li>\r\n	<li>\r\n		菜单管理：用于配置系统菜单，同时也作为权限资源。\r\n	</li>\r\n	<li>\r\n		部门管理：通过不同的部门来管理和区分用户。\r\n	</li>\r\n	<li>\r\n		字典管理：对一些需要转换的数据进行统一管理，如：男、女等。\r\n	</li>\r\n	<li>\r\n		行为日志：用于记录用户对系统的操作，同时监视系统运行时发生的错误。\r\n	</li>\r\n	<li>\r\n		文件上传：内置了文件上传接口，方便开发者使用文件上传功能。\r\n	</li>\r\n	<li>\r\n		代码生成：可以帮助开发者快速开发项目，减少不必要的重复操作，花更多精力注重业务实现。\r\n	</li>\r\n	<li>\r\n		表单构建：通过拖拽的方式快速构建一个表单模块。\r\n	</li>\r\n	<li>\r\n		数据接口：根据业务代码自动生成相关的api接口文档\r\n	</li>\r\n	<li>\r\n		系统参数管理：设置网站一些值，如网站名称等等\r\n	</li>\r\n	<li>\r\n		博客系统管理：包括博文，笔记，标签，评论，类别管理等等\r\n	</li>\r\n	<li>\r\n		定时任务调度模块：更容易开发的定时任务，可以随时管理定时任务的状态，如时间，启动，关闭，是否开机就启动等。（已完成，等待开源）\r\n	</li>\r\n	<li>\r\n		系统环境监测：使用实时查看系统的cpu,内存状态，可以获取系统的各种配置信息包括网卡，MAC等<span>（已完成，等待开源）</span>\r\n	</li>\r\n	<li>\r\n		后台系统查看实时日志：后台用webSocket实现增加实时查看日志功能，再也不用登录linux系统获取日志文件了<span><span>（已完成，等待开源）</span></span> \r\n	</li>\r\n	<li>\r\n		邮箱功能：<span>（已完成收件功能）</span><br />\r\n	</li>\r\n	<li>\r\n		插件化管理：待开发\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	开源\r\n</h1>\r\n<p>\r\n	&nbsp; &nbsp;取于开源，回报开源，本系统所有代码开源及后续的升级开发代码。若你有什么新的需求或者发现了什么BUG，欢迎反馈！！！&nbsp;开源地址：<a href=\"https://gitee.com/linping0124/lazy_bk\">https://gitee.com/linping0124/lazy_bk</a>&nbsp;&nbsp;\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h2>\r\n	<strong>QQ群：</strong> \r\n</h2>\r\n<p>\r\n	321225959\r\n</p>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('29', '2020-02-13 20:14:14', 'website_info_title', '网站说明', '1', '2020-04-14 23:32:49', '<p> <span>一个Java程序员的个人博客，记录博主学习和成长之路，分享Java方面技术和源码</span> </p>', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('30', '2020-02-14 00:05:08', 'frendly_link_info', '友链页面说明', '1', '2020-04-14 23:29:34', '<p> 本站欢迎交换友链，如需交换链接请在留言板留言，博主不定期对友链进行互动访问。 </p> \r\n<h1> 申请格式 </h1> \r\n<div> \r\n <p> <span>名称：小海豚</span> </p> \r\n <p> <span>网址：</span>https://www.nonelonely.com </p> \r\n <p> <span>图标：</span>https://www.nonelonely.com/favicon.ico </p> \r\n <p> <span>描述：</span>一个Java程序员的个人博客，记录博主学习和成长之路。 </p> \r\n</div> \r\n<h1> 申请要求 </h1> \r\n<div> \r\n <i></i> \r\n <span>原创优先</span> \r\n <i></i> \r\n <span>技术优先</span> \r\n <i></i> \r\n <span>经常宕机</span> \r\n <i></i> \r\n <span>不合法规</span> \r\n <i></i> \r\n <span>插边球站</span> \r\n <i></i> \r\n <span>红标报毒</span> \r\n</div> \r\n<p> 希望您在申请本站友链之前请先做好本站链接，申请提交后若无其它额外因素将于72小时内审核，如超过时间还未通过，请私信我。 </p> \r\n<p> 注意本站会不定期清理违规友链，如果检测到该链接已无法正常访问，本站依然会为您保留30天恢复期，逾期则剔除该链接。 </p> \r\n<p> 如若发现互换友链后未经通知私自撤换友链者，本站将拉黑并永不交换，敬请见谅。 </p> \r\n<h1> The End </h1>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('31', '2020-02-14 12:38:30', 'author_info', '个人说明', '1', '2020-04-14 23:28:18', '<h1> 个人信息 </h1> \r\n<p> 网络昵称：小海豚 </p> \r\n<p> 规格：长≈1.8m，净重≈65KG，误差范围±1.5%，数据仅供参考，一切以实物为准 </p> \r\n<p> 产品成分：水约60%，碳水化合物以及脂肪约14%，蛋白质约17%，其他维生素、矿物质、纤维素约9%。 </p> \r\n<p> 原产地：Made in China </p> \r\n<p> 生产批次：95后 </p> \r\n<p> 保质期：长期有效 </p> \r\n<p> 等级：特级 </p> \r\n<p> 外部包装：原装原配，无拆无修 </p> \r\n<p> 产品介绍：猿界小菜鸡，性别男，爱好女，产品已通过国家质量体系认证，手续齐全，稳定运转二十余年不宕机。 </p> \r\n<p> 功能：我们不生产代码，我们只是代码的搬运工。尽量不生产BUG，非可控因素除外 </p> \r\n<p> 适用人群：老少咸宜 </p> \r\n<p> 注意事项：以上信息，最终解释权归厂家所有。 </p> \r\n<h1> 个人介绍 </h1> \r\n<p> 因为爱好促使我选择了计算机专业，也正因为爱好使得了原本枯燥无味的代码增加了两分趣味。 建立博客的目的是想记录自己学习过程中遇到的问题，自己在闲暇之余喜欢练练字，因为练字能使人静下心来远离喧嚣以及内心的浮躁，把练字当做一种放松的方式也算是一种享受。 个人平时喜欢独处，相比于集体环境，我更倾向于独立生活。每个人来到这个世界都是独自来的，所以孤独是一个人的生命本质，学会独处，实际上那是在去学习生命最本质的东西。 独处会让我们有充分的时间思考、完善、精细自己的一切，规划生活的快慢；独处能使人认清自己有多少能量，哪些可为哪些不可为，切实踏实地面对生活学习，一步一个脚印走好每一步。 而思考总是在孤独中产生的，有时候一个人沉思的孤独，看似很心酸，实则更为强大。没有人生来强大，遥远的梦，遥远的你，与其空怀揣梦想，还不如付之行动。 一个人青春的保质期不长，我不想以后为自己的碌碌无为而悔恨，为自己的平庸无奇而懊恼，只要还有梦就应该勇敢去追寻。 优秀的人很多，只有高度自律，才能使自己变得优秀，希望有一天我也能活出自己所期待的样子。像福尔摩斯一样，穿梭街巷观察平时注意不到的细节， 寻觅潜藏在生活里的魔法钥匙。仰望头顶总能见到那盏高悬的月亮，再黯淡的星光也闪烁着来自银河的诗意在流浪，从生活的边角，到远方的故乡。 后来许多人问我一个人夜晚踟蹰路上的心情，我想起的却不是孤单和路长，而是波澜壮阔的海洋和天空中闪耀的星光... </p> \r\n<h1> The End </h1>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('32', '2020-02-14 12:39:02', 'author_name', '个人名称', '1', '2020-04-14 23:28:28', '小海豚', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('33', '2020-02-14 12:39:43', 'author_title', '个人简介', '1', '2020-04-14 23:28:50', '一个朝九晚五的上班族，软件工程专业，善于Java语言。 \r\n<br>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('34', '2020-02-14 12:40:53', 'author_addr', '个人地址', '1', '2020-04-14 23:29:01', '中国-厦门', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('35', '2020-02-14 12:42:19', 'author_tel', '个人联系信息', '1', '2020-04-14 23:32:24', '<div> \r\n <a><strong> \r\n   <table> \r\n    <tbody> \r\n     <tr> \r\n      <td> <span>&lt;div <span>class</span>=\"<span>layui-col-md4 layui-col-md-offset4</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>title</span>=\"<span>QQ</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>onclick</span>=\"<span>var device = layui.device(); </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> if (device.ios || device.android) </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> $(this).attr(\'href\', \'mqqwpa://im/chat?chat_type=wpa&amp;uin=\'+ 207915154 +\'&amp;version=1&amp;src_type=web&amp;web_src=oicqzone.com\'); </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> else </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span><span> $(this).attr(\'href\', \'http://wpa.qq.com/msgrd?v=1&amp;uin=\'+ 207915154 +\'&amp;site=qq&amp;menu=yes\');</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-QQ</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'微信暂时不对外开放，请更换其他联系方式\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-weixin</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'微博链接未进行设置\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-weibo</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'Github链接未进行设置\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-GitHub</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>href</span>=\"<a href=\"mailto:207915154@qq.com\">mailto:207915154@qq.com</a>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-mail</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/div&gt;</span> </td> \r\n     </tr> \r\n    </tbody> \r\n   </table> </strong> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a href=\"mailto:207915154@qq.com\"> <i></i> </a> \r\n</div>', '1', '1', null, '44', null);
INSERT INTO `or_param` VALUES ('36', '2020-02-14 13:26:20', 'is_open_message', '是否开启留言功能', '1', '2020-04-14 23:37:38', '1', '1', '1', null, '45', null);
INSERT INTO `or_param` VALUES ('37', '2020-02-14 22:15:47', 'key_word', '网站关键字', '1', '2020-04-14 23:29:46', 'springboot,个人博客', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('38', '2020-03-13 23:50:08', 'visitor_counts', '网站访问量 计算方法时每天的客户端量加起来。', '1', '2020-04-23 00:30:00', '7519', '1', '1', null, '41', null);
INSERT INTO `or_param` VALUES ('40', '2020-04-13 23:55:01', '初始化参数', '目录', '1', '2020-04-13 23:55:01', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('41', '2020-04-14 21:25:07', '网站设置', '目录', '1', '2020-04-14 21:25:07', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('42', '2020-04-14 21:29:22', '邮箱配置', '目录', '1', '2020-04-14 21:29:22', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('43', '2020-04-14 21:33:41', '登录设置', '目录', '1', '2020-04-14 21:33:41', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('44', '2020-04-14 23:27:42', '关于配置', '目录', '1', '2020-04-14 23:27:42', '', '1', '1', null, '0', null);
INSERT INTO `or_param` VALUES ('45', '2020-04-14 23:36:35', '评论/留言设置', '目录', '1', '2020-04-14 23:36:35', '', '1', '1', null, '0', null);

/*
Navicat MySQL Data Transfer

Source Server         : 119.23.241.23
Source Server Version : 50724
Source Host           : 119.23.241.23:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-04-23 21:15:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for or_scheduled_task
-- ----------------------------
DROP TABLE IF EXISTS `or_scheduled_task`;
CREATE TABLE `or_scheduled_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `init_start_flag` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `task_cron` varchar(255) DEFAULT NULL,
  `task_desc` varchar(255) DEFAULT NULL,
  `task_key` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKd4cjj9eq6nuaxfmlxs8ng8xel` (`create_by`),
  KEY `FK7iy10ellvdfnmneqqsqyk9m9` (`update_by`),
  CONSTRAINT `FK7iy10ellvdfnmneqqsqyk9m9` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKd4cjj9eq6nuaxfmlxs8ng8xel` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of or_scheduled_task
-- ----------------------------
INSERT INTO `or_scheduled_task` VALUES ('1', '2020-04-16 22:49:49', '1', '1', '0 30 0 * * ?', '计算访客量', 'countUv', '2020-04-16 23:30:25', '1', '1');
INSERT INTO `or_scheduled_task` VALUES ('2', '2020-04-17 00:25:09', '1', '1', '0 38 0 * * ?', '删除前3天的Uv', 'deleteUv', '2020-04-19 00:18:16', '1', '1');
INSERT INTO `or_scheduled_task` VALUES ('3', '2020-04-19 18:21:28', '1', '1', '0 0/5 * * * ? ', '定时从邮箱获取数据', 'POP3ReceiveMail', '2020-04-20 20:15:58', '1', '1');
/*
Navicat MySQL Data Transfer

Source Server         : 119.23.241.23
Source Server Version : 50724
Source Host           : 119.23.241.23:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-04-23 21:15:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for or_words
-- ----------------------------
DROP TABLE IF EXISTS `or_words`;
CREATE TABLE `or_words` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk4qjxgbaaeteaayymfthh8bw9` (`create_by`),
  KEY `FKrv13jamughwh48u89vgok5p0j` (`update_by`),
  CONSTRAINT `FKk4qjxgbaaeteaayymfthh8bw9` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKrv13jamughwh48u89vgok5p0j` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of or_words
-- ----------------------------
INSERT INTO `or_words` VALUES ('1', '2020-02-15 20:39:37', '', '1', '爱过的心总会有梦，流过的泪总有痕迹。', '2020-02-23 20:39:54', '1', '1');
INSERT INTO `or_words` VALUES ('2', '2020-02-15 20:40:20', '', '1', '希望源于失望，奋起始于忧患。', '2020-02-23 20:39:45', '1', '1');
INSERT INTO `or_words` VALUES ('3', '2020-02-15 20:41:27', '', '1', '生活是蜿蜒在山中的小径，坎坷不平，沟崖在侧。', '2020-02-23 20:39:37', '1', '1');
INSERT INTO `or_words` VALUES ('4', '2020-02-15 20:42:37', '', '1', '梦虽虚幻，却是自己的梦想。', '2020-02-23 20:39:28', '1', '1');
INSERT INTO `or_words` VALUES ('5', '2020-02-21 15:50:54', '', '1', '很多我们以为一辈子都不会忘记的事情,就在我们念念不忘的日子里,被我们遗忘了。', '2020-02-23 20:39:21', '1', '1');
INSERT INTO `or_words` VALUES ('6', '2020-02-23 20:39:09', '', '1', '穿越千万时间线，只想见你。', '2020-02-23 20:39:09', '1', '1');
INSERT INTO `or_words` VALUES ('7', '2020-03-03 14:04:18', '', '1', '夏蝉冬雪，不过轮回一瞥。', '2020-03-03 14:04:18', '1', '1');
INSERT INTO `or_words` VALUES ('8', '2020-03-04 16:02:18', '', '1', '你可以不用成为我生命中的阳光 但你一定是我的归宿。', '2020-03-04 16:02:18', '1', '1');
INSERT INTO `or_words` VALUES ('9', '2020-03-31 17:32:46', '', '1', '世界上最远的距离 不是瞬间便无处寻觅 而是尚未相遇 便注定无法相聚', '2020-03-31 17:32:46', '1', '1');
INSERT INTO `or_words` VALUES ('10', '2020-04-13 23:02:18', '', '1', '我没有很想你，只是经常梦见你。', '2020-04-13 23:02:18', '1', '1');
/*
Navicat MySQL Data Transfer

Source Server         : 119.23.241.23
Source Server Version : 50724
Source Host           : 119.23.241.23:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-04-23 21:15:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `pids` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKoxg2hi96yr9pf2m0stjomr3we` (`create_by`),
  KEY `FKsiko0qcr8ddamvrxf1tk4opgc` (`update_by`),
  CONSTRAINT `FKoxg2hi96yr9pf2m0stjomr3we` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKsiko0qcr8ddamvrxf1tk4opgc` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '2020-01-09 12:16:13', 'layui-icon layui-icon-home', '0', '[0]', null, '1', '1', '主页', '1', '2020-01-09 12:16:13', '/system/main/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('2', '2020-01-09 12:16:13', 'fa fa-cog', '0', '[0]', null, '2', '1', '系统管理', '1', '2020-01-09 12:16:13', '#', '1', '1');
INSERT INTO `sys_menu` VALUES ('3', '2020-01-09 12:16:13', '', '2', '[0][2]', null, '3', '1', '菜单管理', '2', '2020-01-09 12:16:13', '/system/menu/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('4', '2020-01-09 12:16:13', '', '2', '[0][2]', null, '2', '1', '角色管理', '2', '2020-01-09 12:16:13', '/system/role/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('5', '2020-01-09 12:16:14', '', '2', '[0][2]', null, '1', '1', '用户管理', '2', '2020-01-09 12:16:14', '/system/user/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('6', '2020-02-12 15:21:54', '', '2', '[0],[2]', '', '4', '1', '系统参数管理', '2', '2020-02-12 15:21:54', '/system/param/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('7', '2020-02-12 16:35:45', 'fa fa-files-o', '0', '[0]', '', '3', '1', '博客管理', '1', '2020-02-12 16:35:45', '#', '1', '1');
INSERT INTO `sys_menu` VALUES ('8', '2020-02-12 16:36:24', '', '7', '[0],[7]', '', '1', '1', '博文管理', '2', '2020-02-12 16:36:24', '/system/article/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('9', '2020-02-12 16:38:15', '', '7', '[0],[7]', '', '2', '1', '分类管理', '2', '2020-02-12 16:38:15', '/system/cate/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('10', '2020-02-13 20:29:36', 'fa fa-th', '0', '[0]', '', '4', '1', '开发管理', '1', '2020-02-13 20:29:36', '#', '1', '1');
INSERT INTO `sys_menu` VALUES ('11', '2020-02-13 20:30:02', '', '10', '[0],[10]', '', '1', '1', '代码生成', '2', '2020-02-13 20:30:02', '/dev/code', '1', '1');
INSERT INTO `sys_menu` VALUES ('12', '2020-02-13 20:35:10', '', '7', '[0],[7]', '', '3', '1', '关于管理', '2', '2020-02-13 20:35:10', '/system/about/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('13', '2020-02-14 12:26:50', '', '7', '[0],[7]', '', '4', '1', '友链管理', '2', '2020-02-14 12:26:50', '/system/link/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('14', '2020-02-14 12:48:50', '', '7', '[0],[7]', '', '5', '1', '技能管理', '2', '2020-02-14 12:48:50', '/system/art/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('15', '2020-02-14 12:55:59', '', '7', '[0],[7]', '', '6', '1', '评论/留言管理', '2', '2020-02-14 12:55:59', '/system/comment/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('16', '2020-02-15 20:39:01', '', '7', '[0],[7]', '', '7', '1', '美句管理', '2', '2020-02-15 20:39:01', '/system/words/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('17', '2020-03-12 23:21:07', '', '7', '[0],[7]', '', '8', '1', '流量统计', '2', '2020-03-12 23:21:07', '/system/uv/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('18', '2020-04-16 22:48:18', '', '2', '[0],[2]', '', '5', '1', '定时任务调度', '2', '2020-04-16 22:48:18', '/system/scheduledTask/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('19', '2020-04-16 23:15:49', '', '2', '[0],[2]', '', '6', '1', '字典管理', '2', '2020-04-16 23:15:49', '/system/dict/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('20', '2020-04-19 18:08:07', '', '2', '[0],[2]', '', '7', '1', '邮箱管理', '2', '2020-04-19 18:08:07', '/system/mail/index', '1', '1');
INSERT INTO `sys_menu` VALUES ('21', '2020-04-21 15:46:04', '', '2', '[0],[2]', '', '8', '3', '111', '2', '2020-04-21 15:52:21', '/dev/build', '1', '1');
INSERT INTO `sys_menu` VALUES ('22', '2020-04-22 12:36:54', '', '2', '[0],[2]', '', '8', '1', '部门管理', '2', '2020-04-22 12:36:54', '/system/dept/index', '1', '1');

/*
Navicat MySQL Data Transfer

Source Server         : 119.23.241.23
Source Server Version : 50724
Source Host           : 119.23.241.23:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-04-23 21:29:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKhh52n8vd4ny9ff4x9fb8v65qx` (`role_id`),
  CONSTRAINT `FKb40xxfch70f5qnyfw8yme1n1s` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKhh52n8vd4ny9ff4x9fb8v65qx` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');

